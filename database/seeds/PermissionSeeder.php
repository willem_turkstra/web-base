<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->delete();
      Role::create(['name' => 'Master']);

      DB::table('permissions')->delete();
      Permission::create(['name' => 'Master']);
      Permission::create(['name' => 'Manage Users']);
      Permission::create(['name' => 'Manage Relations']);
    }
}
