<?php

namespace App\Repositories;

use Validator;
use Auth;

class InviteRepository extends Repository implements IRepository {
	public function model(){
		return 'App\Invite';
	}

	public function deleteCompanyInvite($email){
		$company = Auth::user()->company;
		$invite = $this->findByEmailAndCompany($email, $company->id);

		return $this->delete($invite->id);
	}

	public function findByEmailAndCompany($email, $company_id){
		return $this->model->where('email', $email)->where('company_id', $company_id)->first(array('*'));
	}
}
