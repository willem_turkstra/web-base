<?php

namespace App\Repositories;

use Validator;
use Auth;


class UserRepository extends HasPermissionsRepository implements IRepository {

	public function validation(array $data, $id = null){
		return Validator::make($data, [
				'name' => 'required|max:255|min:3',
				'email' => 'required|email|max:255|unique:users,email' . (($id != null) ? ',' . $id : ''),
		]);
	}

	public function validation_password(array $data){
		return Validator::make($data, [
			'password' => 'required|min:6|confirmed',
		]);
	}

	public function validate_invite_form(array $data){
		return Validator::make($data, [
			'name' => 'required|max:255|min:3',
			'password' => 'required|min:6|confirmed'
		]);
	}

	public function validate_invite(array $data,  $id = null){
		return Validator::make($data, [
			'email' => 'required|email|max:255|unique:users,email' . (($id != null) ? ',' . $id : ''),
		]);
	}

	public function update_password($data, $id){
		$data = array('password' => bcrypt($data['password']));
		return $this->update($data, $id);
	}

	public function get_company_users(){
		$company = Auth::user()->company;
		return $company->users;
	}
}
