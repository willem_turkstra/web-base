<?php

namespace App\Repositories;

use App;

abstract class HasPermissionsRepository extends Repository
{
		public function model(){
			return 'App\User';
		}

		protected function HasModelHaveHasPermissionsTrait(){
			$traits = class_uses($this->model());
			if(!empty($traits['Spatie\Permission\Traits\HasRoles'])){
				return true;
			}
			return false;
		}

		public function syncPermissions($id, ...$permissions){
			if($this->HasModelHaveHasPermissionsTrait()){
				$user = $this->model->find($id);
				if(!empty($user)){
					return $user->syncPermissions($permissions);
				}
			}
			return false;
		}
}
