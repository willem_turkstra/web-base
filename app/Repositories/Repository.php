<?php

namespace App\Repositories;

use App;
use Illuminate\Database\Eloquent\Model;

abstract class Repository implements IRepository
{
		protected $app;
		protected $model;

		public function __construct(){
			$this->makeModel();
		}

		abstract function model();

		protected function makeModel(){
			$model = App::make($this->model());

			if(!$model instanceof Model){
				if(!$model instanceof \Illuminate\Foundation\Auth\User){
					throw new \Exception("Model needs to be an instance of Eloquent model or Authenticatable");
				}
			}

			return $this->model = $model;
		}

		public function all($columns = array('*')){
			return $this->model->all($columns);
		}

		public function find($id, $columns = array('*')){
			return $this->model->find($id, $columns);
		}

		public function findBy($attribute, $value, $columns = array('*')){
			return $this->model->where($attribute, $value)->first($columns);
		}

		public function create(array $data){
			return $this->model->create($data);
		}

		public function update(array $data, $id, $attribute = 'id'){
			return $this->model->where($attribute, '=', $id)->update($data);
		}

		public function delete($id){
			return $this->model->destroy($id);
		}
}
