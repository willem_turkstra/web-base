<?php

namespace App\Repositories;

interface IRepository {
	function all($columns = array('*'));
	function find($id, $columns = array('*'));
	function findBy($attribute, $value, $columns = array('*'));
	function create(array $data);
	function update(array $data, $id);
	function delete($id);
}
