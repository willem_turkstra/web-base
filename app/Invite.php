<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Invite extends Model
{
    use Notifiable;

    protected $fillable = ['email', 'code', 'used', 'company_id'];

    public function company(){
        return $this->belongsTo('App\Company');
    }
}
