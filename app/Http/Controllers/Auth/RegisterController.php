<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Company;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $attributeNames = [
          'company.name' => 'company',
          'user.name' => 'name',
          'user.email' => 'email',
          'user.password' => 'password',
        ];
        $validator = Validator::make($data, [
            'company.name' => 'required|min:2',
            'user.name' => 'required|max:255',
            'user.email' => 'required|email|max:255|unique:users,email',
            'user.password' => 'required|min:6|confirmed',
        ]);
        $validator->setAttributeNames($attributeNames);
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $company = new Company;
        $company->name = $data['company']['name'];
        $company->save();
        $company->users()->create([
            'name' => $data['user']['name'],
            'email' => $data['user']['email'],
            'password' => bcrypt($data['user']['password'])
        ]);
        $first_user = $company->users()->first();
        $first_user->assignRole('Master');

        return $first_user;
    }
}
