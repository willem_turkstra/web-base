<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Repositories\UserRepository as Users;
use App\Traits\InviteTrait;

use Input;
use Auth;
use App\Invite;
use App\Company;

class InviteController extends Controller
{
	use InviteTrait;

	public function __construct(Users $users)
	{
			$this->users = $users;
	}

	public function invite(){
		$input = Input::all();

		$validator = $this->users->validate_invite($input);
		if($validator->fails()){
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$this->SendInvite($input['email']);
		return redirect()->back();
	}

	public function form(){
		$input = Input::all();

		if(empty($input['code']) && empty($input['email'])){
			toast()->message(trans('auth.invite_invalid'), 'danger', 'Error');
			return redirect('/');
		}

		$code = $input['code'];
		$email = $input['email'];

		$invite = $this->CheckInvite($input['email'], $input['code']);
		if(!$invite){
			toast()->message(trans('auth.invite_invalid'), 'danger', 'Error');
			return redirect('/');
		}

		$view_data = array();
		$view_data['code'] = $code;
		$view_data['email'] = $email;

		return view('auth.invite_form', $view_data);
	}

	public function accept(){
		$data = Input::all();

		$invite = $this->CheckInvite($data['email'], $data['code']);
		if(!$invite){
			toast()->message(trans('auth.invite_invalid'), 'danger', 'Error');
			return redirect('/');
		}

		$user_data = array(
			'name' => $data['name']
		);
		$user_data = array_merge($user_data, $data['password']);

		$validator = $this->users->validate_invite_form($user_data);
		if($validator->fails()){
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$user_data['email'] = $data['email'];
		$user_data['password'] =  bcrypt($user_data['password']);
		$company = Company::find($invite->company_id);
		$user = $company->users()->create($user_data);

		if(!empty($user)) {
			$invite->used = 1;
			$invite->save();
			toast()->message(trans('auth.successfull_register'), 'success', 'Error');
			return redirect('/login');
		}
		toast()->message(trans('auth.invite_invalid'), 'danger', 'Error');
		return redirect('/');
	}
}
