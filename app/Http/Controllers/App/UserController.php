<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;

use App\Repositories\UserRepository as Users;
use App\Repositories\InviteRepository as Invites;
use Spatie\Permission\Models\Permission;

use App\Http\Requests;
use Auth;
use Datatables;

class UserController extends AppController
{
    protected $view_prefix = 'app.users.';
    private $users;
    private $invites;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Users $users, Invites $invites)
    {
        $this->middleware('auth');
        $this->users = $users;
        $this->invites = $invites;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view_prefix . __FUNCTION__);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $view_data['user'] = $this->users->find($id);
      $view_data['permissions'] = Permission::all();
      return view($this->view_prefix . __FUNCTION__, $view_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      switch($request->input('action')){
        case "user":
          return $this->update_user($request->input('user'), $id);
        break;
        case "password":
          return $this->update_password($request->input('password'), $id);
        break;
        case "permissions":
          return $this->update_permissions($request->input('permissions'), $id);
        break;
      }
    }

    public function update_user($data, $id){

      $validator = $this->users->validation($data, $id);
      if($validator->fails())
      {
        return redirect()
          ->back()
          ->withErrors($validator)
          ->withInput();
      }
      $this->users->update($data, $id);
      return redirect()->route('users.edit', ['user' => $id])->with('message', trans('message.updated', array('object' => trans('user'))));
    }

    public function update_password($data, $id){
      $validator = $this->users->validation_password($data);
      if($validator->fails()){
        return redirect()
          ->back()
          ->withErrors($validator)
          ->withInput();
      }
      $this->users->update_password($data, $id);
      return redirect()->route('users.edit', ['user' => $id])->with('message', trans('message.updated', array('object' => trans('user'))));
    }

    public function update_permissions($data, $id) {
      $this->users->syncPermissions($id, $data);
      return redirect()->route('users.edit', ['user' => $id])->with('message', trans('message.updated', array('object' => trans('user'))));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->users->find($id);
        $this->invites->deleteCompanyInvite($user->email);
        $this->users->delete($id);
        toast()->message(trans('common.delete_success'), 'success', 'Success');
        return redirect()->route('users.index');
    }

    public function table(){
      $company_users = $this->users->get_company_users();

      return Datatables::of($company_users)
      ->addColumn('action', function ($user) {
        $edit_button = (string) view('common.buttons.button', array('href' => route('users.edit', $user->id), 'title' => 'Edit'));
        $delete_button = (string) view('common.buttons.delete', array(
          'href' => route('users.destroy', $user->id),
          'title' => 'Delete',
          'type' => 'btn-danger',
          'confirm' => array(
            'title' => trans('common.confirm_delete'),
            'text' => trans('common.confirm_delete_text', array('object' => trans('user'))),
            'buttontext' => trans('common.confirm_delete_buttontext'),
            'type' => 'warning',
          )
        ));

        if($user->hasRole('Master')){
          return $edit_button;
        }
        else{
          return $edit_button . $delete_button;
        }
      })
      ->make(true);
    }
}
