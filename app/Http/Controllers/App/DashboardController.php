<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends AppController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $view_data = array();
      $view_data['users'] = Auth::user()->company->users;

      return view('app.dashboard', $view_data);
    }
}
