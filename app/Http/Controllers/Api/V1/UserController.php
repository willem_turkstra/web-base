<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;

use App\Repositories\UserRepository as Users;
use App\Traits\InviteTrait;

use Input;

class UserController extends ApiV1Controller
{
	use InviteTrait;

	private $users;

	public function __construct(Users $users)
	{
			$this->users = $users;
	}

	public function index(){
		$company_users = $this->users->get_company_users();
		return response()->json(['users' => $company_users], 200);
	}

	public function invite(){
		$input = Input::all();
		$validator = $this->users->validate_invite($input);
		if($validator->fails()){
			return response()
				->json(['success' => false, 'input' => $input, 'errors' => $validator->errors()]);
		}

		$this->SendInvite($input['email']);

		return response()->json(['success' => true]);
	}

}
