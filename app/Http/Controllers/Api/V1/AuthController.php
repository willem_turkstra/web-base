<?php

namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Request;

class AuthController extends ApiV1Controller
{
    public function authenticate(Request $request)
    {
				$authentication_data = Request::header('authentication');
				$credentials = explode(':', $authentication_data);

				if(count($credentials) != 2){
					return response()->json(['error' => 'invalid_auth_data'], 401);
				}

				$credentials = array(
					'email' => $credentials[0],
					'password' => $credentials[1],
				);

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}
