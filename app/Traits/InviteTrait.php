<?php

namespace App\Traits;

use App\Invite;
use App\User;
use Auth;

use Notification;
use App\Notifications\SendInvite;

trait InviteTrait {
	public function SendInvite($email){
		if(empty(Auth::user()->company->id)){
			toast()->message('No Company on the current user', 'info', 'Info');
			return false;
		}
		$company_id = Auth::user()->company->id;

		$existing_user = User::where('email', $email)->where('company_id', $company_id)->first();
		if($existing_user != null){
			toast()->message(trans('auth.user_already_exists'), 'info', 'Info');
			return redirect()->back();
		}

		$code = md5($email);

		$existing_invite = Invite::where('code', $code)->where('email', $email)->where('company_id', $company_id)->first();
		if($existing_invite != null){
			toast()->message(trans('auth.invite_already_sent'), 'info', 'Info');
			return redirect()->back();
		}

		$invite = Invite::create([
			'email' => $email,
			'code' => $code,
			'company_id' => $company_id
		]);

		if(!empty($invite)){
			Notification::send($invite, new SendInvite($invite));
			toast()->message(trans('auth.invite_sent'), 'info', 'Info');
			return array('email' => $email, 'code' => $code);
		}
		return false;
	}

	public function CheckInvite($email, $code){
		$invite = Invite::where('email', $email)->where('code', $code)->where('used', 0)->first();
		if(!empty($invite)){
			return $invite;
		}
		return false;
	}
}
