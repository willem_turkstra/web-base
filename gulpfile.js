const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 elixir(function(mix) {
     mix.styles([
         'font-awesome.min.css',
         'bootstrap.min.css',
         'datatables.min.css',
         'datatables.bootstrap.min.css',
         'sweetalert2.min.css',
         'checkbox3.min.css',
         'style.css',
         'flat-blue.css',
         'custom.css'
     ]);
 });
 elixir(function(mix) {
     mix.scripts([
         'jquery.js',
         'datatables.min.js',
         'datatables.bootstrap.min.js',
         'bootstrap.min.js',
         'app.js'
     ]);
 });
