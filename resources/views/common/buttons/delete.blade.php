<form method="POST" action="{{ $href }}" accept-charset="UTF-8" data-form-confirm data-confirm-title="{{$confirm['title']}}" data-confirm-text="{{$confirm['text']}}" data-confirm-buttontext="{{$confirm['buttontext']}}" data-confirm-type="{{$confirm['type']}}">
	<input name="_method" type="hidden" value="DELETE">
	{{ csrf_field() }}
	<input class="btn btn-xs {{ $type }}" type="submit" value="{{$title}}" />
</form>
