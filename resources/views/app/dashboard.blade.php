@extends('layouts.app')

@section('content')
<div class="row">
  <?php if(Auth::user()->hasRole('Master') || Auth::user()->hasPermissionTo('Manage users')): ?>
    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
      <a href="{{ url('/users') }}">
        <div class="card blue summary-inline">
          <div class="card-body">
            <i class="icon fa fa-users fa-4x"></i>
            <div class="content">
              <div class="title">{{ $users->count() }}</div>
              <div class="sub-title">Users</div>
            </div>
            <div class="clear-both"></div>
          </div>
        </div>
      </a>
    </div>
  <?php endif; ?>
</div>
@endsection
