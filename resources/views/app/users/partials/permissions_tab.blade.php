

<div role="tabpanel" class="tab-pane" id="user_permissions">
	@if($user->hasRole('Master'))
		The user is a administrator
	@else
	<form class="form-horizontal" role="form" method="POST" action="{{ route('users.update', ['user' => $user->id]) }}">
		{{ csrf_field() }}
		<input name="_method" type="hidden" value="PATCH">
		<input type="hidden" name="action" value="permissions">
		<div class="form-group">
			<label class="col-sm-2 control-label">@lang('common.permissions')</label>
			<div class="col-sm-10">
				@foreach($permissions as $permission)
				<div class="checkbox3 checkbox-inline checkbox-check checkbox-light col-sm-12">
					<input type="checkbox" id="permission-{{$permission->id}}" name="permissions[]" value="{{$permission->name}}"
						@if($user->hasPermissionTo($permission->name))
							checked
						@endif
					/>
					<label for="permission-{{$permission->id}}">
						{{ $permission->name}}
					</label>
				</div>
				@endforeach
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<input type="submit" value="@lang('common.update')" class="btn btn-primary">
			</div>
		</div>
	</form>
	@endif
</div>
