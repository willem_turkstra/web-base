@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">@lang('common.users')</div>
        </div>
        <div class="pull-right card-action">
          <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-gear"></i> <span class="caret"></span></button>
            <ul class="dropdown-menu" role="menu">
              <li><a data-toggle="modal" data-target="#modal_user_invite">Invite new user</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card-body">
        <table id="datatable-table" class="table table-striped datatable" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_user_invite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">@lang('auth.invite_modal_title')</h4>
      </div>
      <form role="form" method="POST" action="{{ url('/invite') }}">
      <div class="modal-body">
          {{ csrf_field() }}
          <div class="control">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="@lang('auth.email')">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
        <button type="submit" class="btn btn-primary">@lang('auth.invite_user')</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
    $('#datatable-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route("users.table") !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'action', name: 'action', orderable: false, searchable: false},
        ],
    });
});
</script>
@endpush
