@extends('layouts.app')

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">@lang('common.users.edit', ['name' => $user->name])</div>
        </div>
      </div>
      <div class="card-body no-padding">
        <div role="tabpanel">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#user_data" aria-controls="user_data" role="tab" data-toggle="tab">@lang('common.user_data')</a></li>
              <?php if(Auth::user()->hasRole('Master') || Auth::user()->hasPermissionTo('Manage users')): ?>
                <li role="presentation"><a href="#user_permissions" aria-controls="user_permissions" role="tab" data-toggle="tab">@lang('common.user_permissions')</a></li>
              <?php endif; ?>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="user_data">
              <div class="sub-title">@lang('common.update_user')</div>
        			<form class="form-horizontal" role="form" method="POST" action="{{ route('users.update', ['user' => $user->id]) }}">
        	      {{ csrf_field() }}
                <input name="_method"  type="hidden" value="PATCH">
        				<input type="hidden" name="action" value="user">
        				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-sm-2 control-label">@lang('common.email')</label>
                  <div class="col-sm-10">
                    {{ old('email') }}
                    <input type="email" name="user[email]" class="form-control" id="email" placeholder="@lang('common.email')" value="@if(!empty(old('email'))) old('email') @else {{ $user->email }} @endif">
                    @if ($errors->has('email'))
        								<span class="help-block">
        										<strong>{{ $errors->first('email') }}</strong>
        								</span>
        						@endif
                  </div>
              	</div>
        				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name" class="col-sm-2 control-label">@lang('common.name')</label>
                  <div class="col-sm-10">
                    <input type="text" name="user[name]" class="form-control" id="name" placeholder="@lang('common.name')" value="{{$user->name}}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                  </div>
              	</div>
        				<div class="form-group">
        					<div class="col-sm-10 col-sm-offset-2">
                  	<input type="submit" value="@lang('common.update')" class="btn btn-primary">
        					</div>
              	</div>
        			</form>
              <div class="sub-title">@lang('common.update_password')</div>
        			<form class="form-horizontal" role="form" method="POST" action="{{ route('users.update', ['user' => $user->id]) }}">
        	      {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">
        				<input type="hidden" name="action" value="password">
        				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="email" class="col-sm-2 control-label">@lang('auth.password')</label>
                  <div class="col-sm-10">
                    <input type="password" name="password[password]" class="form-control" id="email" placeholder="@lang('auth.password')" >
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
              	</div>
        				<div class="form-group">
                  <label for="name" class="col-sm-2 control-label">@lang('auth.confirm_password')</label>
                  <div class="col-sm-10">
                    <input type="password" name="password[password_confirmation]" class="form-control" id="name" placeholder="@lang('auth.confirm_password')">
                  </div>
              	</div>
        				<div class="form-group">
        					<div class="col-sm-10 col-sm-offset-2">
                  	<input type="submit" value="@lang('common.update')" class="btn btn-primary">
        					</div>
              	</div>
        			</form>
            </div>
            <?php if(Auth::user()->hasRole('Master') || Auth::user()->hasPermissionTo('Manage users')): ?>
              @include('app.users.partials.permissions_tab', ['user' => $user, 'permissions' => $permissions])
            <?php endif; ?>
          </div>
        </div>
      </div>
		</div>
  </div>
</div>
@endsection
