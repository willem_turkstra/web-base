<!DOCTYPE html>
<html>

  <head>
    <title>Admin - {{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS App -->
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
  </head>
  <body class="flat-blue">
    <div class="app-container">
      <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-expand-toggle">
                <i class="fa fa-bars icon"></i>
              </button>
              <ol class="breadcrumb navbar-breadcrumb">
                <li class="active">Dashboard</li>
              </ol>
              <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                <i class="fa fa-th icon"></i>
              </button>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                  <i class="fa fa-times icon"></i>
              </button>
              <li class="dropdown profile">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                <ul class="dropdown-menu animated fadeInDown">
                  <li>
                    <div class="profile-info">
                      <h4 class="username">{{ Auth::user()->company->name }}</h4>
                      <p>{{ Auth::user()->name }}</p>
                      <p>{{ Auth::user()->email }}</p>
                      <div class="btn-group margin-bottom-2x" role="group">
                        <form role="form" method="POST" action="{{ url('/logout') }}">
                          {{ csrf_field() }}
                          <button type="submit" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</button>
                        </form>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
        <div class="side-menu">
          <nav class="navbar navbar-default" role="navigation">
            <div class="side-menu-container">
              <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/dashboard')}}">
                  <div class="icon fa fa-paper-plane"></div>
                  <div class="title">{{ config('app.name') }}</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                  <i class="fa fa-times icon"></i>
                </button>
              </div>
              <ul class="nav navbar-nav">
                <?php if(Auth::user()->hasRole('Master') || Auth::user()->hasPermissionTo('Manage users')): ?>
                  <li>
                    <a href="{{ url('/users') }}">
                      <span class="icon fa fa-users"></span><span class="title">Users</span>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            </div>
          </nav>
        </div>
        <div class="container-fluid">

          <div class="side-body padding-top">
            @include('toast::messages')
            @yield('content')
          </div>
        </div>
      </div>
      <footer class="app-footer">
        <div class="wrapper">
          <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright.
        </div>
      </footer>
    </div>
    <div>
      <script src="{{ elixir('js/all.js') }}"></script>
      <script src="/js/sweetalert2.min.js"></script>
      <script src="/js/alerts.js"></script>
      @stack('scripts')
    </div>
  </body>
</html>
