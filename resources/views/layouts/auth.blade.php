<!DOCTYPE html>
<html>

  <head>
    <title>Flat Admin V.2 - Free Bootstrap Admin Templates</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS App -->
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">
  </head>

  <body class="flat-blue login-page">
    <div class="container">
      @include('toast::messages')
  		@yield('content')
  	</div>
    <script src="{{ elixir('js/all.js') }}"></script>
  </body>
</html>
