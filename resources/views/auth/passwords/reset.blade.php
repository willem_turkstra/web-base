@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div>
    <div class="login-form row">
      <div class="col-sm-12 text-center login-header">
        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
        <h4 class="login-title">{{ config('app.name')}}</h4>
      </div>
      <div class="col-sm-12">
        <div class="login-body">
          <form role="form" method="POST" action="{{ url('/password/reset') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="control">
              <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus placeholder="@lang('auth.email')">
            </div>
            <div class="control">
              <input id="password" type="password" class="form-control" name="password" placeholder="@lang('auth.password')">
            </div>
            <div class="control">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@lang('auth.confirm_password')">
            </div>
            <div class="login-button text-center">
              <input type="submit" class="btn btn-primary" value="@lang('auth.reset')">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
