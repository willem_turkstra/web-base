@extends('layouts.auth')

<!-- Main Content -->
@section('content')
<div class="login-box">
  <div>
    <div class="login-form row">
      <div class="col-sm-12 text-center login-header">
        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
        <h4 class="login-title">{{ config('app.name')}}</h4>
      </div>
      <div class="col-sm-12">
        <div class="login-body">
          @if (session('status'))
              <div class="alert alert-success">
                  {{ session('status') }}
              </div>
          @endif
          <form role="form" method="POST" action="{{ url('/password/email') }}">
            {{ csrf_field() }}
            <div class="control">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="@lang('auth.email')">
            </div>
            <div class="login-button text-center">
              <button type="submit" class="btn btn-primary">
                  @lang('auth.send_password_reset_link')
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
