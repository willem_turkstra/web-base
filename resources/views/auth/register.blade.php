@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div>
    <div class="login-form row">
      <div class="col-sm-12 text-center login-header">
        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
        <h4 class="login-title">{{ config('app.name')}}</h4>
      </div>
      <div class="col-sm-12">
        <div class="login-body">
          <form role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}

            <div class="control">
              <b>@lang('auth.company')</b>
            </div>
            <div class="control{{ $errors->has('user.name') ? ' has-error' : '' }}">
              <input id="name" type="text" class="form-control" name="company[name]" value="{{ old('company.name') }}" autofocus placeholder="@lang('auth.company.name')">
              @if ($errors->has('company.name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('company.name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="control">
              <b>@lang('auth.user')</b>
            </div>

            <div class="control{{ $errors->has('user.name') ? ' has-error' : '' }}">
              <input id="name" type="text" class="form-control" name="user[name]" value="{{ old('name') }}" autofocus placeholder="@lang('auth.name')">
              @if ($errors->has('user.name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('user.name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="control{{ $errors->has('user.email') ? ' has-error' : '' }}">
							<input id="email" type="email" class="form-control" name="user[email]" value="{{ old('user.email') }}" autofocus placeholder="@lang('auth.email')">
              @if ($errors->has('user.email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('user.email') }}</strong>
                  </span>
              @endif
            </div>
            <div class="control {{ $errors->has('user.password') ? ' has-error' : '' }}">
							<input id="password" type="password" class="form-control" name="user[password]" value="{{ old('user.password') }}" placeholder="@lang('auth.password')">
              @if ($errors->has('user.password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('user.password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="control">
              <input id="password-confirm" type="password" class="form-control" name="user[password_confirmation]" value="{{ old('user.password_confirmation') }}" placeholder="@lang('auth.confirm_password')">
            </div>
            <div class="login-button text-center">
              <input type="submit" class="btn btn-primary" value="@lang('auth.register')">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
