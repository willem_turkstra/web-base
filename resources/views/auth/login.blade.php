@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div>
    <div class="login-form row">
      <div class="col-sm-12 text-center login-header">
        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
        <h4 class="login-title">{{ config('app.name')}}</h4>
      </div>
      <div class="col-sm-12">
        <div class="login-body">
            @foreach($errors->all() as $error)
              <div class="alert alert-danger">{{ $error }}</div>
            @endforeach
          <form role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="control">
							<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus placeholder="@lang('auth.email')">
            </div>
            <div class="control">
							<input id="password" type="password" class="form-control" name="password" placeholder="@lang('auth.password')">
            </div>
            <div class="login-button text-center">
              <input type="submit" class="btn btn-primary" value="@lang('auth.login')">
            </div>
          </form>
        </div>
        <div class="login-footer">
          <div class="checkbox">
              <label>
                  <input type="checkbox" name="remember"> Remember Me
              </label>
          </div>
          <span class=""><a class="color-black" href="{{ url('/password/reset') }}">@lang('auth.forgot_password')</a></span>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
