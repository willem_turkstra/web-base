@extends('layouts.auth')

@section('content')
<div class="login-box">
  <div>
    <div class="login-form row">
      <div class="col-sm-12 text-center login-header">
        <i class="login-logo fa fa-connectdevelop fa-5x"></i>
        <h4 class="login-title">{{ config('app.name')}}</h4>
      </div>
      <div class="col-sm-12">
        <div class="login-body">
          <form role="form" method="POST" action="{{ url('/invite/accept') }}">
            {{ csrf_field() }}

            <div class="control">
              <b>@lang('auth.company')</b>
							<b>{{ $email }}</b>
            </div>
            <input type="hidden" name="code" value="{{ $code }}" />
            <input type="hidden" name="email" value="{{ $email }}" />
            <div class="control">
              <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus placeholder="@lang('auth.name')">
            </div>
            <div class="control">
              <input id="password" type="password" class="form-control" name="password[password]" placeholder="@lang('auth.password')">
            </div>
            <div class="control">
              <input id="password-confirm" type="password" class="form-control" name="password[password_confirmation]" placeholder="@lang('auth.confirm_password')">
            </div>
            <div class="login-button text-center">
              <input type="submit" class="btn btn-primary" value="@lang('auth.register')">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
