<?php

return [
	'save' => 'Save',
	'update' => 'Update',
	'edit' => 'Edit',

	'data' => 'Data',
	'update_user' => 'Update User data',
	'update_password' => 'Change Password',


	'email' => 'Email',
	'name' => 'Name',

	'users' => 'Users',
	'users.edit' => 'Edit user :name',

	'confirm_delete' => 'Delete!',
	'confirm_delete_text' => 'Are you sure you want to delete this :object?',
	'confirm_delete_buttontext' => 'Yes!',
	'close' => 'Close',

];
