<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'user' => 'User',
    'company' => 'Company',
    'company.name' => 'Company',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'login' => 'Login',
    'register' => 'Register',
    'reset' => 'Reset password',
    'forgot_password' => 'Forgot password?',
    'send_password_reset_link' => 'Send Password Reset Link',
    'invite_modal_title' => 'Invite a new user',
    'invite_user' => 'Invite',
    'invite_already_sent' => 'An invite has already been sent to this emailadres.',
    'invite_sent' => 'Invite has been sent',
    'successfull_register' => 'You have been successfully registered',
];
