<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1', 'middleware' => 'cors'], function(){
  Route::get('token', 'AuthController@authenticate');

  Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('protected', 'AuthController@protectedData');
    Route::get('users', 'UserController@index');
    Route::post('invite', 'UserController@invite');
  });
});
