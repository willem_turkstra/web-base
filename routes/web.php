<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'App'], function () {
  Route::get('/dashboard', 'DashboardController@index');
  Route::get('/users/table', 'UserController@table')->name('users.table');
  Route::resource('users', 'UserController');
});

Route::group(['namespace' => 'Auth'], function(){
  //invite
  Route::post('/invite', 'InviteController@invite');
  Route::get('/invite/form', 'InviteController@form');
  Route::post('/invite/accept', 'Invitecontroller@accept');
});
