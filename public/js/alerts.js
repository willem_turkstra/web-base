$(document).ready(function(){
  confirm_init();
});

function confirm_init(){
  console.log('test');

  $(document).on('submit', 'form[data-form-confirm]', function(event){
    console.log('test1');
    event.preventDefault();

    var form_obj = $(this);
    var swal_obj = confirm(form_obj);

    swal_obj.then(function() {
      $(document).off("submit", 'form[data-form-confirm]')
      form_obj.submit();
    }, function(dismiss) {
      //do nothing, the user has canceled.
    });
  });
}

function confirm(object){
  var title = "";
  var text = "";
  var buttontext = "";
  var type = "";

  if(object.attr('data-confirm-title')) {
    title = object.attr('data-confirm-title');
  } else {
    throw "A title for the alert should be given.";
  }
  if(object.attr('data-confirm-text')){
     text = object.attr('data-confirm-text');
  } else {
    throw "A text for the alert should be given.";
  }
  if(object.attr('data-confirm-buttontext')){
    buttontext = object.attr('data-confirm-buttontext');
  } else {
    throw "A buttontext for the alert should be given.";
  }
  if(object.attr('data-confirm-type')){
    type = object.attr('data-confirm-type');
  } else {
    throw "A type for the alert should be given.";
  }

  return swal({
    title: title,
    text: text,
    type: type,
    showCancelButton: true,
    cancelButtonColor: '#d33',
    confirmButtonText: buttontext,
  });
}
