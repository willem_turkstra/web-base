(function() {
  'use strict';
  angular.module('app.users').controller('UsersController', UsersController);

  UsersController.$inject = ['$scope', '$state', 'UsersService', '$ionicLoading', '$ionicModal'];

  function UsersController($scope, $state, UsersService, $ionicLoading, $ionicModal){
    var vm = this;
    vm.users = [];
    vm.reload = reload;
    vm.invite_modal = undefined;

    init();

    function init() {
      load_users();
      init_invite_modal();
  	}

    function reload(){
      UsersService.index().then(function(data){
        vm.users = data.users;
      });
      $scope.$broadcast('scroll.refreshComplete');
    }

    function load_users(){
      $ionicLoading.show({
        template: 'Loading...',
      }).then(function(){
        console.log("The loading indicator is now displayed");
      });
      UsersService.index().then(function(data){
        vm.users = data.users;
        $ionicLoading.hide().then(function(){
         console.log("The loading indicator is now hidden");
        });

      });
    }

    function init_invite_modal(){
      console.log('test');
      $ionicModal.fromTemplateUrl('templates/users/invite-modal.html', {
        animation: 'slide-in-up'
      }).then(function(modal) {
        vm.invite_modal = modal;
      });
    }
  }
})();
