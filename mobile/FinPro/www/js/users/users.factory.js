(function() {
  'use strict';

  angular.module('app.users').factory('UsersService', UsersService);

  UsersService.$inject = ['$q', '$http', 'url'];

  function UsersService($q, $http, url){
    var service = {
			index: index,
      invite: invite
    };

    return service;

		function index(){
			var deferred = $q.defer();
			$http.get(url + 'users').
			success(function(data, status, headers, config) {
				deferred.resolve(data);
			}).
			error(function(data, status, headers, config) {
				deferred.resolve(false);
			});
			return deferred.promise;
		}
    function invite(data){
      var deferred = $q.defer();
      $http.post(url + 'invite', data).
      success(function(data, status, headers, config){
        	deferred.resolve(data);
      }).
      error(function(){
        deferred.resolve(false);
      });
      return deferred.promise;
    }
  }
})();
