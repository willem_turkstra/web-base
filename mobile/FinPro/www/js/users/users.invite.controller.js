(function() {
  'use strict';
  angular.module('app.users').controller('UsersInviteController', UsersInviteController);

  UsersInviteController.$inject = ['$scope', '$state', 'UsersService', '$ionicLoading'];

  function UsersInviteController($scope, $state, UsersService, $ionicLoading){
    var vm = this;
		vm.invite = invite;
		vm.email = '';
    vm.modal = undefined;

    init();

    function init() {
  	}

		function invite(){
      $ionicLoading.show({
        template: 'Sending invite...'
      });
      UsersService.invite({email: vm.email}).then(function(data){
				if($scope.$parent.modal){
          $scope.$parent.modal.hide()
        }
        $ionicLoading.hide();
			});
		}
  }
})();
