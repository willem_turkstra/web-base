(function() {
  'use strict';

  angular.module('app.users').run(UsersRoutes);

  UsersRoutes.$inject = ['routerHelper'];

  function UsersRoutes(routerHelper) {
    routerHelper.configureStates(GetStates());
  }

  function GetStates() {
    return [
      {
        state: 'app.users',
        config: {
          url: '/users',
          views: {
            'menuContent': {
              templateUrl: 'templates/users/users.html',
              controller: 'UsersController',
              controllerAs: 'vm',
            }
          }
        }
      }
    ];
  }
})();
