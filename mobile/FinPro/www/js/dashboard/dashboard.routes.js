(function() {
  'use strict';

  angular.module('app.dashboard').run(DashboardRoutes);

  DashboardRoutes.$inject = ['routerHelper'];

  function DashboardRoutes(routerHelper) {
      var otherwise = '/app/dashboard';
      routerHelper.configureStates(GetStates(), otherwise);
  }

  function GetStates() {

    return [
      {
        state: 'app.dashboard',
        config: {
          url: '/dashboard',
          views: {
            'menuContent': {
              templateUrl: 'templates/dashboard/dashboard.html',
            }
          },
          data: {
            requireLogin: true
          }
        }
      }
    ];
  }
})();
