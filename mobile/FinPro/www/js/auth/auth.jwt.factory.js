(function(){
    'use strict';

    angular.module('app.auth').factory('AuthService', AuthService);

    AuthService.$inject = ['$window', 'jwtHelper'];

    function AuthService($window, jwtHelper){
        var KEY_TOKEN = 'token';

        var service = {
          token: "",
          setToken: setToken,
          getToken: getToken,
          checkIfTokenIsSet: checkIfTokenIsSet,
          isTokenExpired: isTokenExpired,
          //isTokenValid: isTokenValid,
          preformFullTokenCheck: preformFullTokenCheck,
          getPayload: getPayload
        };

        return service;

        function setToken(token){
      		$window.sessionStorage.setItem(KEY_TOKEN, token);
      	}
      	function getToken(){
      		return $window.sessionStorage.getItem(KEY_TOKEN);
      	}
      	function checkIfTokenIsSet(){
      		if(service.getToken()){
      			return true;
      		}
      		return false;
      	}
      	function isTokenExpired(){
      		if(service.checkIfTokenIsSet()){
      			return false;
      		}
      		return jwtHelper.isTokenExpired(service.token);
      	}
      	/*function isTokenValid(){
      		if(!service.checkIfTokenIsSet()){
      			return false;
      		}
      		var payload = service.getPayload();

          console.log(payload);
      		if(payload) {
      			return (payload.jti == CONSUMER_KEY);
      		}
      	}*/
      	function preformFullTokenCheck(){
      		if(service.checkIfTokenIsSet()){
      			if(!service.isTokenExpired()){
      				//if(service.isTokenValid()){
      					return true;
      			//	}
      			}
      		}
      		return false;
      	}
      	function getPayload(){
      		if(!service.checkIfTokenIsSet()){
      			return false;
      		}
      		try{
      			return jwtHelper.decodeToken(service.getToken());
      		}
      		catch(err){
      			return false;
      		}
      		return false;
      	}
    }
})();
