(function () {
    'use strict';

    angular.module('app.auth').controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', 'LoginService', '$state', '$ionicLoading'];

    function LoginController($scope, LoginService, $state, $ionicLoading) {
      var vm = this;
      vm.login = login;
      vm.user = {
          email: '',
          password: ''
      };

      function login() {
        $ionicLoading.show({
          template: 'Loggin in...',
        });
      	LoginService.token(vm.user.email, vm.user.password).then(function(value) {
          if(value !== false){
        	   $state.go('app.dashboard');
          }
          $ionicLoading.hide();
      	});
      }
    }
})();
