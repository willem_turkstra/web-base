(function(){
  'use strict';

  angular.module('app.auth', ['app.core', 'angular-jwt'])


  .config(function($httpProvider, jwtOptionsProvider, jwtInterceptorProvider){
    $httpProvider.interceptors.push('jwtInterceptor');
    $httpProvider.interceptors.push('AuthResponseInterceptor');

    jwtOptionsProvider.config({
      whiteListedDomains: ['192.168.178.192:1234'],
      tokenGetter: ['AuthService', function(AuthService) {
        return AuthService.getToken();
      }]
    });
  });
})();
