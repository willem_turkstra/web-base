(function(){
    'use strict';

    angular.module('app.auth').factory('LoginService', LoginService);

    LoginService.$inject = ['$http', '$q', 'url', 'AuthService'];

    function LoginService($http, $q, url, AuthService){
      	var service = {
          token: token,
          register: register
        };

        return service;

      	function token(email, pass, complete_callback){
      		var deferred = $q.defer();
      		$http.defaults.headers.common.Authentication = email + ':' + pass;
      		$http({
      			url: url + 'token',
      			skipAuthorization: true,
      			method: 'GET'
      		}).
      		success(function(data, status, headers, config) {
      			AuthService.setToken(data.token);
      			deferred.resolve(data.token);
      		}).
      		error(function(data, status, headers, config) {
      			deferred.resolve(false);
      		});

      		return deferred.promise;
      	}

      	function register(new_user){
      		var deferred = $q.defer();

      		$http({
      			url: url + 'register',
      			data: new_user,
      			skipAuthorization: true,
      			method: 'POST'
      		}).
      		success(function(data, status, headers, config) {
      			deferred.resolve(true);
      		}).
      		error(function(data, status, headers, config) {
      			deferred.resolve(false);
      		});

      		return deferred.promise;
      	}
    }
})();
