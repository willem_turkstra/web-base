
(function() {
    'use strict';

    angular.module('app.auth').run(AuthRoutes);

    AuthRoutes.$inject = ['routerHelper', '$rootScope', '$state', 'AuthService'];

    function AuthRoutes(routerHelper, $rootScope, $state , AuthService) {
        routerHelper.configureStates(GetStates(), 'login');


        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {


          $rootScope.isCollapsed = true;

          if(toState.data){
            var requireLogin = toState.data.requireLogin;
            $rootScope.isAuthenticated = AuthService.preformFullTokenCheck();

            if (requireLogin && !$rootScope.isAuthenticated) {
              $state.go('login');
              event.preventDefault();
            }
            if(toState.name == 'login' && $rootScope.isAuthenticated && requireLogin == false)
            {
              $state.go('app.dashboard');
            }
          }
        });
    }

    function GetStates() {
        return [
            {
                state: 'login',
                config: {
                    url: '/login',
                    templateUrl: 'templates/auth/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm',
                    data: {
                      requireLogin: false
                    }
                }
            }
        ];
    }
})();
