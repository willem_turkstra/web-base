(function(){
    'use strict';

    angular.module('app.auth').factory('AuthResponseInterceptor', AuthResponseInterceptor);

    AuthResponseInterceptor.$inject = ['$q', '$injector'];

    function AuthResponseInterceptor($q, $injector){
			return {
				response: function (response) {
          return response || $q.when(response);
      	},
      	responseError: function (response) {
          if (response.status === 401 || response.status === 400) {
						var stateService = $injector.get('$state');
						stateService.go('login');
          }
          return $q.reject(response);
      	}
			}
		}
})();
