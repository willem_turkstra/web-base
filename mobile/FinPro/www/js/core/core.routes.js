(function() {
  'use strict';

  angular.module('app.core').run(CoreRoutes);

  CoreRoutes.$inject = ['routerHelper'];

  function CoreRoutes(routerHelper) {
      var otherwise = '/app/dashboard';
      //routerHelper.configureStates(GetStates(), otherwise);
  }

  function GetStates() {
      return [
          {
              state: 'app.notfound',
              config: {
                  url: '/notfound',
                  views: {
                    'menuContent': {
                      templateUrl: 'templates/core/404.html',

                    }
                  }
              }
          }
      ];
  }
})();
